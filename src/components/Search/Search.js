import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actionCreators from '../../store/actions'
import { sendDataRequest } from '../../services/ApiService'
import Container from '../styles/Container'
import Input from '../styles/Input'
import SearchButton from '../styles/SearchButton'

class Search extends Component {
  constructor(props) {
    super(props)
    this.queryRef = React.createRef()
    this.query = ''
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleButtonClick = this.handleButtonClick.bind(this)
  }

  async handleInputChange(e) {
    await this.props.updateQuery(e.target.value)
  }

  async handleButtonClick(e) {
    e.preventDefault()
    if (this.queryRef.current.value.length > 1) {
      const newSearchResponse = await sendDataRequest(this.props.searchStore.query, 0, this.props.toggleLoading, this.props.updateResults, this.props.updatePagination)
      if(newSearchResponse) {
        await this.props.updateTempQuery(this.props.searchStore.query)
        await this.props.updatePaginationBar({
          offset: 0,
          startPage: 1
        })
      } else {
        await this.props.updateTempQuery(this.props.searchStore.query)
      }
    }
  }

  render() {
    return (
      <Container>
        <Input 
          ref={this.queryRef} 
          onChange={e => this.handleInputChange(e)} 
          value={this.props.searchStore.query} 
        />
        <SearchButton onClick={e => this.handleButtonClick(e)}>Search</SearchButton>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    searchStore: state.search,
    paginationStore: state.pagination,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateQuery: (data) => dispatch(actionCreators.updateQuery(data)),
    updateTempQuery: (data) => dispatch(actionCreators.updateTempQuery(data)),
    toggleLoading: (bool) => dispatch(actionCreators.toggleLoading(bool)),
    updateResults: (data) => dispatch(actionCreators.updateResults(data)),
    updatePagination: (data) => dispatch(actionCreators.updatePagination(data)),
    updatePaginationBar: (data) => dispatch(actionCreators.updatePaginationBar(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)

Search.propTypes = {
  searchStore: PropTypes.object,
  paginationStore: PropTypes.object,
  updateQuery: PropTypes.func,
  updateTempQuery: PropTypes.func,
  toggleLoading: PropTypes.func,
  updateResults: PropTypes.func,
  updatePagination: PropTypes.func,
  handleInputChange: PropTypes.func,
  handleButtonClick: PropTypes.func
}
