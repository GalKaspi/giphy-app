import React, { Component } from 'react'
import './App.css'
import { connect } from 'react-redux'
import * as actionCreators from '../store/actions'
import PropTypes from 'prop-types'
import Header from './Header/Header'
import PaginationBar from './PaginationBar/PaginationBar'
import GifDisplay from './GifDisplay/GifDisplay'
import { sendDataRequest } from '../services/ApiService'
import RootContainer from './styles/RootContainer'
import Paragraph from './styles/Paragraph'
import Wrapper from './styles/Wrapper'

class App extends Component {

  async componentDidMount() {
    await sendDataRequest(this.props.searchStore.query, 0, this.props.toggleLoading, this.props.updateResults, this.props.updatePagination)
  }

  render() {

    return (
      <RootContainer>
        {this.props.appStore.loading ? <Paragraph padded>Loading</Paragraph> : 
        <Wrapper>
          <Header />
          {this.props.appStore.results.length > 0 && <Wrapper>
            <PaginationBar />
            <GifDisplay results={this.props.appStore.results} />
          </Wrapper>}
          {this.props.appStore.results.length === 0 && <Paragraph padded>{`No GIFs found for ${this.props.searchStore.tempQuery} :(`}</Paragraph>}
        </Wrapper>}
      </RootContainer>
    )
  }

}

const mapStateToProps = state => {
  return {
    appStore: state.app,
    searchStore: state.search
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toggleLoading: (bool) => dispatch(actionCreators.toggleLoading(bool)),
    updateResults: (data) => dispatch(actionCreators.updateResults(data)),
    updatePagination: (data) => dispatch(actionCreators.updatePagination(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

App.propTypes = {
  appStore: PropTypes.object,
  toggleLoading: PropTypes.func,
  updateResults: PropTypes.func,
  updatePagination: PropTypes.func
}