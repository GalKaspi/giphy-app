import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actionCreators from '../../store/actions'
import { sendDataRequest } from '../../services/ApiService'
import Container from '../styles/Container'
import Paragraph from '../styles/Paragraph'
import Span from '../styles/Span'
import PaginationButton from '../styles/PaginationButton'

class PaginationBar extends Component {
  constructor(props) {
    super(props)
    this.offset = 0
    this.getResults = this.getResults.bind(this)
  }

  async updateQueryParams(direction) {
    if(direction === 'next') {
      this.offset = this.props.paginationStore.offset + 10
      await this.props.updatePaginationBar({
        offset: this.offset,
        startPage: this.props.paginationStore.startPage + 10
      })
    } else {
      this.offset = this.props.paginationStore.offset - 10
      await this.props.updatePaginationBar({
        offset: this.offset,
        startPage: this.props.paginationStore.startPage - 10
      })
    }
  }

  async getResults(direction) {
    await this.updateQueryParams(direction)
    let sendQuery
    if(this.props.searchStore.query.length > 0) {
      sendQuery = this.props.searchStore.query
    } else {
      sendQuery = this.props.searchStore.tempQuery
    }
    await sendDataRequest(sendQuery, this.offset, this.props.toggleLoading, this.props.updateResults, this.props.updatePagination)
  }

  render() {
    return (
      <Container padded>
        <Paragraph>Showing {this.props.paginationStore.startPage} - {this.props.paginationStore.endPage} results of total {this.props.paginationStore.totalGifs} for 
        "<Span>{this.props.searchStore.tempQuery}</Span>"</Paragraph>
        {this.props.paginationStore.startPage >= 11 && 
            <PaginationButton onClick={() => this.getResults('prev')}>PREV</PaginationButton>}
        {this.props.paginationStore.endPage >= 10  && this.props.paginationStore.endPage < this.props.paginationStore.totalGifs &&
          <PaginationButton onClick={() => this.getResults('next')} >NEXT</PaginationButton>}
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    paginationStore: state.pagination,
    searchStore: state.search,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updatePaginationBar: (data) => dispatch(actionCreators.updatePaginationBar(data)),
    toggleLoading: (bool) => dispatch(actionCreators.toggleLoading(bool)),
    updateResults: (data) => dispatch(actionCreators.updateResults(data)),
    updatePagination: (data) => dispatch(actionCreators.updatePagination(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaginationBar)

PaginationBar.propTypes = {
  searchStore: PropTypes.object,
  paginationStore: PropTypes.object,
  updatePagination: PropTypes.func,
  updatePaginationBar: PropTypes.func,
  toggleLoading: PropTypes.func,
  updateResults: PropTypes.func
}
