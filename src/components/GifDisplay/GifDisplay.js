import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy } from '@fortawesome/free-solid-svg-icons'
import Container from '../styles/Container'
import Subtitle from '../styles/Subtitle'
import Card from '../styles/Card'
import Image from '../styles/Image'
import Link from '../styles/Link'

const GifDisplay = (props) => {
  const { results } = props
  return (
    <Container padded>
      {results.map((result) => {
        return (
          <Card key={result.id}>
            <Image 
              src={result.images.fixed_height_small.url} 
              heightStyle={`${result.images.fixed_height_small.height}px`} 
              alt={result.title} 
            />
            <Subtitle widthStyle={`${result.images.fixed_height_small.width}px`}>{result.title}</Subtitle>
            <Link> <FontAwesomeIcon icon={faCopy} className='copy-icon-fa'/> Copy URL</Link>
          </Card>
        )
      })}
    </Container>
  )
}

GifDisplay.propTypes = {
  results: PropTypes.array,
}

export default GifDisplay
