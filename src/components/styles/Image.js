import styled from 'styled-components'

export default styled.img`
    height: ${props => props.heightStyle || "200px"};
    margin-bottom: 0.5rem;
`