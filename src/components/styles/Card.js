import styled from 'styled-components'

export default styled.div`
    display: flex;
    flex-direction: column;
    flex: 1 0 calc(16.66% - 20px);
    -webkit-box-shadow: 5px 5px 9px 0px rgba(224,224,224,0.7);
    -moz-box-shadow: 5px 5px 9px 0px rgba(224,224,224,0.7);
    box-shadow: 5px 5px 9px 0px rgba(224,224,224,0.7);
    margin: 0 2rem 2rem 0;
    padding: 0 1rem 1rem 0;
    height: calc(100% - 20px);
    max-width: 200px;
    box-sizing: border-box;
    text-align: center;

    &:hover {
        -webkit-box-shadow: 8px 8px 18px 0px rgba(200,200,200,0.7);
        -moz-box-shadow: 8px 8px 18px 0px rgba(200,200,200,0.7);
        box-shadow: 8px 8px 18px 0px rgba(200,200,200,0.7);
    }
`