import styled from 'styled-components'
import Container from './Container'

export default styled(Container)`
    background: #333;
    padding: 1rem;
    height: 80px;
    box-sizing: border-box;
`