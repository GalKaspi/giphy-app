import styled from 'styled-components'

export default styled.a`
    margin: 0;
    color: #777;
    font-weight: 400;
    font-size: 0.9rem;
    padding-top: 0.4rem;
    cursor: pointer;
`