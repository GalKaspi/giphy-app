import styled from 'styled-components'

export default styled.input`
    border-top-left-radius: 0.6rem;
    border-bottom-left-radius: 0.6rem;
    background: #fff;
    border: 2px solid #07be07;
    padding: 0 0.4rem 0 0.8rem;
    font-size: 1.1rem;
    height: 38px;
    box-sizing: border-box;
`