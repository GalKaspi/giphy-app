import styled from 'styled-components'

export default styled.p`
    margin: 0;
    color: #000;
    font-weight: 400;
    font-size: 1.3rem;
    padding: ${prop => (prop.padded ? '1rem 0' : '0')};
`