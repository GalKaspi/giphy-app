import styled from 'styled-components'

export default styled.button`
    border-top-right-radius: 0.5rem;
    border-bottom-right-radius: 0.5rem;
    background: #07be07;
    border: 2px solid #07be07;
    padding: 0.5rem 1.5rem;
    font-size: 1rem;
    color: #fff;
    height: 38px;
    box-sizing: border-box;
    cursor: pointer;
`