import styled from 'styled-components'
import Paragraph from './Paragraph';

export default styled(Paragraph)`
    text-transform: uppercase;
    font-size: 0.9rem;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    width: ${props => props.widthStyle || "200px"};
    margin: 0 auto;
`