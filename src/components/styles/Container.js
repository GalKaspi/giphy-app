import styled from 'styled-components'

export default styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    justify-content: ${prop => (prop.spaced ? 'space-between' : 'flex-start')};
    padding: ${prop => (prop.padded ? '2.5rem 1rem 0 1rem' : '0')};
    font-family: 'Roboto', sans-serif;
`