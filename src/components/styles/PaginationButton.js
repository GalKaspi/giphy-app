import styled from 'styled-components'

export default styled.button`
    background: #d5d5d5;
    border: 0;
    padding: 0.2rem 0.3rem;
    font-size: 0.75rem;
    color: #000;
    outline: none;
    border-radius: 0.3rem;
    margin: 0 0.3rem 0 0.4rem;
    font-family: 'Roboto', sans-serif;
    box-sizing: border-box;
    cursor: pointer;
`