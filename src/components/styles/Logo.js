import styled from 'styled-components'

export default styled.h1`
    margin: 0;
    font-family: 'Alfa Slab One', cursive;
    color: #fff;
    font-weight: 100;
    font-size: 1.9rem;
    letter-spacing: 1px;
`