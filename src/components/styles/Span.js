import styled from 'styled-components'

export default styled.span`
    font-weight: 600;
    display: inline-block;
`