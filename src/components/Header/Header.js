import React from 'react'
import Search from '../Search/Search'
import HeaderContainer from '../styles/Header'
import Logo from '../styles/Logo'

const Header = () => {

  return (
    <HeaderContainer spaced>
      <Logo>Giphy Browser</Logo>
      <Search />
    </HeaderContainer>
  )
}

export default Header
