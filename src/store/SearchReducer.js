import * as actionTypes from './actions'

const initialState = {
    query: 'puppies',
    tempQuery: 'puppies'
}

const SearchReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_QUERY:
      return Object.assign({}, state, {
        query: action.payload
      })
    case actionTypes.UPDATE_TEMP_QUERY:
      return Object.assign({}, state, {
        tempQuery: action.payload
      })
    default: return state
  }
}

export default SearchReducer