export const TOGGLE_LOADING = 'TOGGLE_LOADING'
export const UPDATE_RESULTS = 'UPDATE_RESULTS'
export const UPDATE_PAGINATION = 'UPDATE_PAGINATION'
export const UPDATE_PAGINATION_BAR = 'UPDATE_PAGINATION_BAR'
export const UPDATE_QUERY = 'UPDATE_QUERY'
export const UPDATE_TEMP_QUERY = 'UPDATE_TEMP_QUERY'

export const toggleLoading = (bool) => {
    return {
        type: TOGGLE_LOADING,
        payload: bool
    }
}

export const updateResults = (data) => {
    return {
        type: UPDATE_RESULTS,
        payload: data
    }
}

export const updatePagination = (data) => {
    return {
        type: UPDATE_PAGINATION,
        payload: data
    }
}

export const updatePaginationBar = data => {
    return {
        type: UPDATE_PAGINATION_BAR,
        payload: data
    }
}

export const updateQuery = data => {
    return {
        type: UPDATE_QUERY,
        payload: data
    }
}

export const updateTempQuery = data => {
    return {
        type: UPDATE_TEMP_QUERY,
        payload: data
    }
}