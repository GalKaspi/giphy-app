import * as actionTypes from './actions'

const initialState = {
    loading: true,
    results: []
}

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_LOADING:
      return Object.assign({}, state, {
        loading: action.payload
      })
    case actionTypes.UPDATE_RESULTS:
      return Object.assign({}, state, {
        results: action.payload
      })
    default: return state
  }
}

export default AppReducer