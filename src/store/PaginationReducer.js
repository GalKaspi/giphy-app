import * as actionTypes from './actions'

const initialState = {
    startPage: 1,
    endPage: 10,
    offset: 0,
    totalGifs: 0
}

const PaginationReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_PAGINATION_BAR:
      return Object.assign({}, state, {
        startPage: action.payload.startPage,
        offset: action.payload.offset
      })
    case actionTypes.UPDATE_PAGINATION:
      return Object.assign({}, state, {
        totalGifs: action.payload.totalGifs,
        endPage: action.payload.endPage
      })
    default: return state
  }
}

export default PaginationReducer