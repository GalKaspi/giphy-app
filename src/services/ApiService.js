//const API_KEY = process.env.REACT_APP_GIPHY_API_KEY
const API_KEY = 'OzzpC2Omr6OvUM3FYFuHsDUIEXshpGDB'
const API_URL = 'http://api.giphy.com/v1/gifs/search'

let sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

let getData = async (query, offset) => {
  const response = await fetch(`${API_URL}?api_key=${API_KEY}&q=${query}&limit=10&offset=${offset}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin'
  })
  let responseJson = await response.json()
  if (response.status === 200) {
    return responseJson
  }
}

let sendDataRequest = async (query, offset, toggleLoading, updateResults, updatePagination) => {
  toggleLoading(true)
  const resultsData = await getData(query, offset)
  if(resultsData && resultsData.data.length > 0) {
    updateResults(resultsData.data)
    let newEndPage
    if(resultsData.pagination.total_count >= 10 && resultsData.pagination.total_count > offset + 10) {
      newEndPage = offset + 10
    } else {
      newEndPage = resultsData.pagination.total_count
    }
    updatePagination({
      totalGifs: resultsData.pagination.total_count,
      endPage: newEndPage
    })
    await sleep(1000)
    toggleLoading(false)
    return true
  } else {
    updateResults(resultsData.data)
    toggleLoading(false)
    return false
  }
}

export {
  getData,
  sendDataRequest
}